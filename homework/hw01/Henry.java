import javax.swing.*;
import java.awt.*;

public class Henry {
    private JFrame frame;

    Henry(){
        frame = new JFrame();
        frame.setSize(1000,400);
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);

        frame.getContentPane().setLayout(new GridLayout(2,2));
        JTabbedPane tp = new JTabbedPane(JTabbedPane.TOP);

        tp.addTab("Search by Author", new SearchByAuthorPanel());
        tp.addTab("Search by Category", new SearchByCategoryPanel());
        tp.addTab("Search by Publisher", new SearchByPublisherPanel());

        frame.getContentPane().add(tp);

        frame.setVisible(true);
    }

    public static void main(String[] args) {
        new Henry();
    }
}