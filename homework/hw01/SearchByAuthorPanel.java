import javax.swing.*;
import java.awt.*;
import java.awt.event.*;

public class SearchByAuthorPanel extends JPanel {

    private JComboBox author;
    private JComboBox book;
    private JTextField price;
    private JTextArea branchInfo;
    private String[] books = {"flkafjdlf", "kdlfjlskjf", "ldkjfalskjf", "fldkjflksjf"};
    private String[] authors = {"Test1", "Test2", "Test3"};

    public SearchByAuthorPanel() {
        JPanel mainPanel = new JPanel();
        mainPanel.setLayout(new GridLayout(1,2));

        // this list will be populated from the database


        String priceString = "$1000000.00";

        author = new JComboBox(authors);
        ActionListener authorChange = new AuthorChangeListener();
        author.addActionListener(authorChange);
        book = new JComboBox(books);
        book.addActionListener(authorChange);
        price = new JTextField();
        price.setText(priceString);
        price.setEditable(false);

        branchInfo = new JTextArea(5,20);
        branchInfo.setEditable(false);

        add(author);
        add(book);
        add(price);
        add(branchInfo);
    }


    private class AuthorChangeListener implements ActionListener {
        public void actionPerformed(ActionEvent e) {

            System.out.println("Author: " + authors[author.getSelectedIndex()] + "    Book: " + books[book.getSelectedIndex()]);


        }
    }
}
