import javax.swing.*;
import java.awt.*;

public class SearchByPublisherPanel extends JPanel {

    private JComboBox author;
    private JComboBox book;

    public SearchByPublisherPanel() {
        JPanel mainPanel = new JPanel();
        mainPanel.setLayout(new GridLayout(2,2));


        // this list will be populated from the database
        String[] authors = {"Test1", "Test2", "Test3"};
        String[] books = {"flkafjdlf", "kdlfjlskjf", "ldkjfalskjf", "fldkjflksjf"};

        author = new JComboBox(authors);
        book = new JComboBox(books);

        add(author);
        add(book);
    }
}